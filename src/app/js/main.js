import {UniApp} from "./entities/UniApp.js";

/**
 * export main functionality in global namespace
 */
window.UniApp  = UniApp;

document.addEventListener("DOMContentLoaded", function () {
    let APP = window.APP = new UniApp();
});
