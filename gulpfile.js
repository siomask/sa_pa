'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    watchify = require("watchify"),
    notify = require('gulp-notify'),
    autoprefixer = require('gulp-autoprefixer'),
    del = require('del'),
    cssnano = require('gulp-cssnano'),
    merge = require('event-stream').merge,
    concat = require('gulp-concat'),
    reload = browserSync.reload
    ;


function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
var path = {
    build: {
        name: 'bundle',
        path: 'build/',
        img: 'build/assets/images/'
    },
    src: {
        style: ['src/app/**/*.scss', 'src/libs/**/*.scss']
    },
    out: {
        style: {origin: 'style.css', hash: guidGenerator() + '.css'},
    },
    watch: {
        style: ['src/app/**/*.scss', 'src/libs/**/*.scss', 'src/app/**/*.sass']
    },
    clean: './build'
};


gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});


gulp.task('style:build', function () {
    var paths = "./src/lib/styles/",
        csslibs = gulp.src([])
            .pipe(concat('vendors.css'));

    var sassS = gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass({

            includePaths: ['src/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }).on('error', sass.logError))
        .pipe(cssmin())
        .pipe(cssnano({zindex: false}))
        .pipe(autoprefixer({
            browsers: ['last 16 versions'],
            cascade: false
        }));


    return merge(sassS, csslibs)
        .pipe(concat(path.build.name + ".css"))
        //.pipe(concat(path.out.style.hash))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.path))
        .pipe(notify({message: 'Styles task complete'}))
        .pipe(reload({stream: true}));
});



gulp.task('clean', function () {
    return del.sync([path.build.path])
});

gulp.task('build', [
    'clean',
    'style:build'
]);


gulp.task('watch', function () {
    watch(path.watch.style, function (event, cb) {
        gulp.start('style:build');
    });
});


gulp.task('default', ['build', 'watch']);