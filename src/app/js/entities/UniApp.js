/**
 * Class representing a main action.
 */
export class UniApp {
    /**
     *@constructor
     *
     */
    constructor() {

        let self = this;
        this.selectedTab = 1;

        (function () {
            let mainApp = self.container = document.querySelector('.main-app'),
                tabsContainer = self.tabsContainer = mainApp.querySelector('.tabs-head-items'),
                tabsItemsBody = mainApp.querySelector('.tab-body'),
                tabsHeadItems = mainApp.querySelector('.tabs-head'),
                _t = self.tabsBody = tabsItemsBody.querySelector('.tab-body-content-track'),
                activeTabClassName = 'active';


            (mainApp.addEventListener || mainApp.attachEvent).bind(mainApp)('click', (e)=> {
                let target = e.target,//|| e.srcElement,
                    _dataSet = target.dataset,
                    dataAction = target.getAttribute('data-mobile-action'),
                    dataEditMobile = target.getAttribute('data-edit-mobile'),
                    dataEdit = target.getAttribute('data-edit'),
                    tabIndex = target.getAttribute('data-link-to');
                // console.log(e.target.dataset,tabIndex);

                if (_dataSet) {
                    tabIndex = _dataSet.linkTo;
                    dataEdit = _dataSet.edit;
                    dataEditMobile = _dataSet.editMobile;
                }
                if (tabIndex) {
                    self.selectedTab = tabIndex;
                    self.updateTabItem();
                }
                if (dataEditMobile) {
                    target.style.display = 'none';
                    target.parentNode.querySelector('.tab-btns').style.display = 'flex';

                    self.lastEdit = [];
                    [].forEach.call(tabsItemsBody.querySelectorAll('.item-to-edit'), (el)=> {
                        let _data = el.children[0],
                            _d = _data.querySelector('.data-edit');
                        if (_d) {
                            el.style.display = 'none';
                            let edit = document.createElement('div');
                            edit.className = 'input-container';
                            edit.innerHTML =
                                '<label>' + _d.getAttribute('data-label') + '</label>' +
                                '<input value="' + _d.children[0].innerText + '"/>';
                            el.parentNode.appendChild(edit);
                            self.checkInput(edit.children[1]);
                            self.lastEdit.push({
                                editTempDom: edit,
                                domEl: _d
                            });
                        }

                    });

                } else if (dataAction) {


                    [].forEach.call(tabsItemsBody.querySelectorAll('.item-to-edit'), (el)=> {
                        el.style.display = '';
                    });
                    while (self.lastEdit.length) {
                        let el = self.lastEdit.shift();
                        switch (dataAction) {
                            case 'save':
                            {
                                let _v = el.editTempDom.querySelector('input').value;
                                [].forEach.call(self.container.querySelectorAll("." + el.domEl.classList[0]), (el)=> {
                                    if (el.querySelector('span')) el.querySelector('span').innerText = _v;
                                });

                                break;
                            }
                        }

                        el.editTempDom.parentNode.removeChild(el.editTempDom);
                    }
                    self.lastSelected.parentNode.querySelector('.tab-btns').style.display = '';
                } else if (self.lastSelected && self.lastSelected.getAttribute('data-edit-mobile')) {
                    return
                }


                if (dataEdit) {
                    let _div = target.parentNode.children[0],
                        _d = _div.querySelector('div');
                    self.editPopUp.onSelect(e, {
                        label: _d.dataset.label,
                        text: _d.children[0]
                    });
                    self.checkInput(self.editPopUp.input);
                    target.style.display = 'none';
                } else {
                    if (self.editPopUp.container.contains(e.target) && e.target.localName != 'button')return;
                    self.editPopUp.hide();
                    if (self.lastSelected)self.lastSelected.style.display = '';
                    e.stopPropagation();
                    e.preventDefault();
                }

                self.lastSelected = target;
            })

            let windowHandler = (window.addEventListener || window.attachEvent).bind(window),
                mainAppHandler = (mainApp.addEventListener || mainApp.attachEvent).bind(mainApp);
            windowHandler('resize', (e)=> {
                self.recalcTabs(_t);
                self.updateTabsBody();
            });
            windowHandler('focus', (e)=> {
                if (e.target.localName == 'input') {
                    e.target.parentNode.className += ' inp-focused';
                }
            }, true);
            mainAppHandler('blur', (e)=> {
                if (e.target.localName == 'input') {
                    e.target.parentNode.className = e.target.parentNode.className.replace(' inp-focused', '');
                }
            }, true);
            mainAppHandler('input', (e)=> {
                if (e.target.localName == 'input') {
                    self.checkInput(e.target);
                }
            }, true);
            setTimeout(()=> {
                self.initSwipe(tabsItemsBody,(dir)=>{
                    self.selectedTab = parseInt(self.selectedTab) +dir;
                    self.updateTabItem();
                });
                self.initSwipe(tabsHeadItems,(dir)=>{
                    let _w = tabsHeadItems.children[0].children[0].clientWidth,
                        last = 0;
                    // if( tabsHeadItems.children[0].style.transform)last = parseInt(tabsHeadItems.children[0].style.transform.match(/\d+/g)[0]);
                    tabsHeadItems.children[0].style.transform='translateX('+(dir>0?tabsHeadItems.clientWidth *-1 :0)+'px)';
                });
                self.recalcTabs(_t);
                self.editPopUp = new EditPopUp(self);
            }, 100)
        })()


    }

    updateTabItem() {
        let _tabs = this.tabsContainer.children;
        if (this.selectedTab > _tabs.length)this.selectedTab = _tabs.length;
        if (this.selectedTab < 1)this.selectedTab = 1;
        let activeTabClassName = 'active',
            _act = this.tabsContainer.querySelector("." + activeTabClassName),
            _next = this.tabsContainer.children[this.selectedTab - 1];
        _act.className = _act.className.replace(" " + activeTabClassName, "");
        _next.className += " " + activeTabClassName;
        this.updateTabsBody();
    }

    updateTabsBody() {
        this.tabsBody.style.transform = `translateX(${-(this.tabsBody.parentNode.clientWidth) * (this.selectedTab - 1)}px)`;
    }

    recalcTabs(domElem) {
        let container = domElem,
            width = 0,
            padding = 2 * parseInt(window.getComputedStyle(container.children[0])['paddingLeft']),
            parentWidth = container.parentNode.clientWidth - padding;
        container.style.width = (container.children.length + 2) * parentWidth + 'px';
        [].forEach.call(container.children, function (el) {
            el.style.width = `${parentWidth}px`;
            // el.style.transform = `translateX(${width}px)`;
            // width+=parentWidth;
        })
    }

    initSwipe(domEl,callback) {
        let touchstartX = 0;
        let touchstartY = 0;
        let touchendX = 0;
        let touchendY = 0;
        let self = this;

        const gestureZone = domEl;

        gestureZone.addEventListener('touchstart', function (event) {
            touchstartX = event.changedTouches[0].screenX;
            touchstartY = event.changedTouches[0].screenY;
        }, false);

        gestureZone.addEventListener('touchend', function (event) {
            touchendX = event.changedTouches[0].screenX;
            touchendY = event.changedTouches[0].screenY;
            handleGesture();
        }, false);

        function handleGesture() {
            if (Math.abs(touchendX - touchstartX) < 2)return;

            callback((touchendX < touchstartX ? 1 : -1));

        }
    }

    checkInput(input) {
        input.parentNode.className = input.value ? (!input.parentNode.className.match('input-has-value') ? input.parentNode.className + ' input-has-value' : input.parentNode.className) : input.parentNode.className.replace(' input-has-value', '');
    }
}

class EditPopUp {
    constructor(app) {
        let container = app.container.querySelector('edit-pop-up'),
            _container = this.container = document.createElement('div');
        container.parentNode.appendChild(_container);
        container.parentNode.removeChild(container);
        _container.innerHTML = '' +
            '<form action="">' +
            '   <div class="input-container">' +
            '      <label></label>' +
            '      <input/>' +
            '   </div>' +
            '   <div class="form-btns">' +
            '      <button class="btn btn-blue btn-fill" data-save-item="true"><span>Save</span></button>' +
            '      <button class="btn btn-blue"><span>Cancel</span></button>' +
            '   </div>' +
            '</form>' +
            '<div class="triangle-with-shadow"></div> ' +
            '';

        _container.className = 'popup popup-edit';
        this.input = _container.querySelector('input');
        this.label = _container.querySelector('label');
        let from = _container.querySelector('form');
        (from.addEventListener || from.attachEvent).bind(from)('click', (e)=> {

            if (e.target.localName.toLowerCase() == 'button') {


                if (e.target.dataset.saveItem) {
                    [].forEach.call(app.container.querySelectorAll("." + this.opt.text.parentNode.classList[0]), (e)=> {
                        e.querySelector('span').innerText = this.input.value;
                    })

                    this.opt.text.innerText = this.input.value;
                }
            }
        }, true)
    }

    hide() {
        this.container.style.display = '';
    }

    onSelect(e, opt) {
        this.opt = opt;
        this.container.style.display = 'block';
        this.container.style.left = e.clientX + 'px';
        this.container.style.top = (e.clientY - 50) + 'px';

        this.input.value = opt.text.innerText;
        this.label.innerText = opt.label || 'Some Label';

    }

}


